var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/*
  Generated class for the NewsService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var NewsService = (function () {
    function NewsService(http) {
        this.http = http;
        console.log('Hello NewsService Provider');
    }
    NewsService.prototype.load = function () {
        var _this = this;
        if (this.data) {
            // already loaded data
            return Promise.resolve(this.data);
        }
        // don't have the data yet
        return new Promise(function (resolve) {
            _this.http.get('https://newsapi.org/v1/articles?source=google-news&apiKey=0ec9503599a349f3beced9edb558646a')
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                _this.data = data.articles;
                resolve(_this.data);
            });
        });
    };
    return NewsService;
}());
NewsService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http])
], NewsService);
export { NewsService };
//# sourceMappingURL=news-service.js.map