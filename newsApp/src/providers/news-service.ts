import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the NewsService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class NewsService {

	public data: any;
  constructor(public http: Http) {
    console.log('Hello NewsService Provider');
  }

  load() {
  if (this.data) {
    // already loaded data
    return Promise.resolve(this.data);
  }

  // don't have the data yet
  return new Promise(resolve => {
    this.http.get('https://newsapi.org/v1/articles?source=google-news&apiKey=0ec9503599a349f3beced9edb558646a')
      .map(res => res.json())
      .subscribe(data => {
        this.data = data.articles;
        resolve(this.data);
      });
  });
}

	loadNews(category: string){
		if (this.data) {
    	// already loaded data
    	return Promise.resolve(this.data);
  		}

	  	if(category == 'Technology'){
			return new Promise(resolve => {
	    this.http.get('https://newsapi.org/v1/articles?source=ars-technica&apiKey=0ec9503599a349f3beced9edb558646a')
	      .map(res => res.json())
	      .subscribe(data => {
	        this.data = data.articles;
	        resolve(this.data);
	      });
	  	});
		} else if(category == 'General'){
			return new Promise(resolve => {
	    this.http.get('https://newsapi.org/v1/articles?source=bbc-news&apiKey=0ec9503599a349f3beced9edb558646a')
	      .map(res => res.json())
	      .subscribe(data => {
	        this.data = data.articles;
	        resolve(this.data);
	      });
	  	});
		} else if(category == 'Sports'){
			return new Promise(resolve => {
	    this.http.get('https://newsapi.org/v1/articles?source=bbc-sport&apiKey=0ec9503599a349f3beced9edb558646a')
	      .map(res => res.json())
	      .subscribe(data => {
	        this.data = data.articles;
	        resolve(this.data);
	      });
	  	});
		} else if(category == 'Business'){
			return new Promise(resolve => {
	    this.http.get('https://newsapi.org/v1/articles?source=bloomberg&apiKey=0ec9503599a349f3beced9edb558646a')
	      .map(res => res.json())
	      .subscribe(data => {
	        this.data = data.articles;
	        resolve(this.data);
	      });
	  	});
		} else if(category == 'Entertainment'){
			return new Promise(resolve => {
	    this.http.get('https://newsapi.org/v1/articles?source=mashable&apiKey=0ec9503599a349f3beced9edb558646a')
	      .map(res => res.json())
	      .subscribe(data => {
	        this.data = data.articles;
	        resolve(this.data);
	      });
	  	});
		} else if(category == 'Music'){
			return new Promise(resolve => {
	    this.http.get('https://newsapi.org/v1/articles?source=mtv-news&apiKey=0ec9503599a349f3beced9edb558646a')
	      .map(res => res.json())
	      .subscribe(data => {
	        this.data = data.articles;
	        resolve(this.data);
	      });
	  	});
		} else if(category == 'Gaming'){
			return new Promise(resolve => {
	    this.http.get('https://newsapi.org/v1/articles?source=ign&apiKey=0ec9503599a349f3beced9edb558646a')
	      .map(res => res.json())
	      .subscribe(data => {
	        this.data = data.articles;
	        resolve(this.data);
	      });
	  	});
		} else if(category == 'Science & Nature'){
			return new Promise(resolve => {
	    this.http.get('https://newsapi.org/v1/articles?source=national-geographic&apiKey=0ec9503599a349f3beced9edb558646a')
	      .map(res => res.json())
	      .subscribe(data => {
	        this.data = data.articles;
	        resolve(this.data);
	      });
	  	});
		}

	}

}
