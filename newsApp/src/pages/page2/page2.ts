import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { NewsService } from '../../providers/news-service';

import { SocialSharing, InAppBrowser } from 'ionic-native';

@Component({
  selector: 'page-page2',
  templateUrl: 'page2.html',
  providers: [NewsService]
})
export class Page2 {
  
  public title: string;
  public news: any;
  public browser: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public newsService: NewsService) {
    // If we navigated to this page, we will have an item available as a nav param
    // this.selectedItem = navParams.get('item');
    this.title = navParams.data;
    this.loadNews(this.title);
    // Let's populate this page with some filler content for funzies
    // this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    // 'american-football', 'boat', 'bluetooth', 'build'];

    // this.items = [];
    // for (let i = 1; i < 11; i++) {
    //   this.items.push({
    //     title: 'Item ' + i,
    //     note: 'This is item #' + i,
    //     icon: this.icons[Math.floor(Math.random() * this.icons.length)]
    //   });
    // }
  }

  loadNews(category: string){
    this.newsService.loadNews(category)
    .then(data => {
        this.news = data;
      });
  }

  itemTapped() {
    // That's right, we're pushing to ourselves!
    // this.navCtrl.push(Page2, {
    //   item: item
    // });
    this.navCtrl.popToRoot();
  }

  rootPage(){
    this.navCtrl.popToRoot();
  }

  twitterShare(image: any, url: any){
    SocialSharing.shareViaTwitter("Share through News App",image,url);
  }

  instagramShare(image: any, url: any){
    SocialSharing.shareViaInstagram("Shared through News App", image);
  }

  whatsappShare(image: any, url: any){
    SocialSharing.shareViaWhatsApp("Share through News App", image, url);
  }

  openBrowser(url){
    let options = 'location=yes,toolbar=yes,hidden=no';
    this.browser = new InAppBrowser(url,'_blank',options);
    // this.browser.show();
  }

}
