import { Component, ViewChild } from '@angular/core';
declare var cordova: any;
import { NavController, Platform } from 'ionic-angular';

import { NewsService } from '../../providers/news-service';

import { InAppBrowser } from 'ionic-native';


@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html',
  providers: [NewsService]
})

export class Page1 {
  
	public news: any;
  public features: any;
  public browser: any;
  // public cordova: any;
  testSlides: string[] = [];
  @ViewChild('mySlider')
  mySlider: any;

  constructor(public navCtrl: NavController, 
  			public newsService: NewsService, public platform: Platform) {
  		this.loadNews();

        this.features = {
        slidesPerView:1,
        pager: true,
      // nextButton: ".swiper-button-next",
      // prevButton: ".swiper-button-prev",        
        onInit:()=>{
        }
     }
  setTimeout(()=>{
        for (var i=1; i<6; i++) {
            this.testSlides.push("Slide - "+i);
          }
   },100);

 }

  loadNews(){
  	this.newsService.load()
  	.then(data => {
  		this.news = data;
  	});
  }

  openBrowser(url){
    // let options = 'location=no,toolbar=yes,hidden=no';
    this.browser = window.open(url,'_blank','location=yes');
    // this.browser.show();
  }
}
